package test

import (
	"fmt"
	"testing"

	"gitlab.com/erlanggalaimena/two-sum/service"
)

func TestTwoSum(t *testing.T) {
	fmt.Println("Test Case 1 Processed")
	testCase1 := service.TwoSum([]int{2, 7, 11, 15}, 9)
	if testCase1[0] != 0 && testCase1[1] != 1 {
		t.Error("Test Case 1 Not Pass")
	} else {
		fmt.Println("Test Case 1 Passed")
	}

	fmt.Println("Test Case 2 Processed")
	testCase2 := service.TwoSum([]int{3, 2, 4}, 6)
	if testCase2[0] != 1 && testCase2[1] != 2 {
		t.Error("Test Case 2 Not Pass")
	} else {
		fmt.Println("Test Case 2 Passed")
	}

	fmt.Println("Test Case 3 Processed")
	testCase3 := service.TwoSum([]int{3, 3}, 6)
	if testCase3[0] != 0 && testCase3[1] != 1 {
		t.Error("Test Case 3 Not Pass")
	} else {
		fmt.Println("Test Case 3 Passed")
	}
}
