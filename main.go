package main

import (
	"fmt"

	"gitlab.com/erlanggalaimena/two-sum/service"
)

func main() {
	fmt.Println(service.TwoSum([]int{2, 7, 11, 15}, 9))
	fmt.Println(service.TwoSum([]int{3, 2, 4}, 6))
	fmt.Println(service.TwoSum([]int{3, 3}, 6))
}
