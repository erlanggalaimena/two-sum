package service

func TwoSum(nums []int, target int) []int {
	var tempMap map[int]int = make(map[int]int)

	for index, num := range nums {
		if tempMap[target-num] == 0 {
			tempMap[num] = index + 1
		} else {
			return []int{tempMap[target-num] - 1, index}
		}
	}

	return nil
}
